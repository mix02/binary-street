import { controls } from '../../constants/controls';
import { getRandomNumber } from '../helpers/apiHelper';
import { fighterHealthIndicator } from './fightersView';

export async function fight(firstFighter, secondFighter) {
  let { health: healthFirst } = firstFighter;
  let { health: healthSecond } = secondFighter;

  const maxHealthFirst = healthFirst;
  const maxHealthSecond = healthSecond;

  // console.log(healthFirst, healthSecond);

  const fighters = {
    firstFighter,
    secondFighter,
    healthFirst,
    healthSecond,
    maxHealthFirst,
    maxHealthSecond
  };

  return new Promise((resolve) => {
    return document.addEventListener('keydown', battleAction(resolve, fighters));
  });
}

function battleAction(
  resolve,
  { firstFighter, secondFighter, healthFirst, healthSecond, maxHealthFirst, maxHealthSecond }
) {
  return function (event) {
    event.preventDefault();

    switch (event.code) {
      case controls['PlayerOneAttack']:
        const firstDamage = getDamage(firstFighter, secondFighter);
        healthSecond = lostHealth(healthSecond, firstDamage);
        break;
      case !controls['PlayerOneAttack'] && controls['PlayerOneBlock']:
        const firstBlock = getBlockPower(firstFighter);
        healthFirst = recoveryHealth(healthFirst, firstBlock, maxHealthFirst);
        break;
      case controls['PlayerTwoAttack']:
        const secondDamage = getDamage(secondFighter, firstFighter);
        healthFirst = lostHealth(healthFirst, secondDamage);
        break;
      case controls['PlayerTwoBlock'] && !controls['PlayerTwoAttack']:
        const secondBlock = getBlockPower(secondFighter);
        healthSecond = recoveryHealth(healthSecond, secondBlock, maxHealthSecond);
        break;
      case controls['PlayerOneAttack'] && controls['PlayerTwoBlock']:
        break;
      case controls['PlayerTwoAttack'] && controls['PlayerOneBlock']:
        break;
      default:
        console.log('TYTYYTYTYTYYTOGOGOOT');
    }

    const playerFirstCombo = getComboResults(event.code, controls['PlayerOneCriticalHitCombination']);
    const playerSecondCombo = getComboResults(event.code, controls['PlayerTwoCriticalHitCombination']);

    if (playerFirstCombo) {
      const comboDamage = ultimateAttack(firstFighter, 2);
      healthSecond = lostHealth(healthSecond, comboDamage);
    }

    if (playerSecondCombo) {
      const comboDamage = ultimateAttack(secondFighter, 2);
      healthFirst = lostHealth(healthFirst, comboDamage);
    }

    console.log('Health 1 ', healthFirst, ' Health 2 ', healthSecond);

    fighterHealthIndicator('left-fighter-indicator', healthFirst, maxHealthFirst);
    fighterHealthIndicator('right-fighter-indicator', healthSecond, maxHealthSecond);

    if (healthFirst === 0) {
      resolve(secondFighter);
    } else if (healthSecond === 0) {
      resolve(firstFighter);
    }
  };
}

function ultimateAttack(fighter, mul) {
  const { attack } = fighter;
  return mul * attack;
}

function lostHealth(health, damage) {
  health = health - damage > 0 ? health - damage : 0;

  return health;
}

function recoveryHealth(health, block, maxHealth) {
  health = health + block <= maxHealth ? health + block : maxHealth;

  return health;
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  if (blockPower > hitPower) {
    return 0;
  }

  return hitPower - blockPower;
}

export function getHitPower(fighter) {
  const { attack } = fighter;

  const criticalHitChance = getRandomNumber(1, 2);
  const power = attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;

  const dodgeChance = getRandomNumber(1, 2);
  const power = defense * dodgeChance;

  return power;
}

function getComboResults(code, codes) {
  let pressed = new Set();

  pressed.add(code);
  // console.log(codes)
  for (let code of codes) {
    if (pressed.has(code)) {
      return true;
    }
  }

  pressed.clear();

  return false;
}

// runOnKeys(
//   () => alert("Привет!"),
//   "KeyQ",
//   "KeyW"
// );
