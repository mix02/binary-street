import { createElement } from "../../helpers/domHelper";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  // call showModal function 

  const { name: title } = fighter;


  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  bodyElement.innerText = 'YOU WIN!';

  const args = {
    title, 
    bodyElement
  };


  showModal(args);

}
