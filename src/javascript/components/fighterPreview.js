import { createElement } from '../helpers/domHelper';
import { isEmpty } from '../helpers/apiHelper';
export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)

  if (!isEmpty(fighter) && fighter !== null && fighter !== undefined) {
    const imgElement = createFighterImage(fighter);
    const fighterParams = createFighterParams(fighter);

    fighterElement.append(imgElement, ...fighterParams);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

function createFighterParams(fighter) {
  const { name, health, attack, defense } = fighter;

  const titleElement = createElement({
    tagName: 'figcaption',
    className: 'fighter-preview___title'
  });

  const healthElement = createElement({
    tagName: 'div',
    className: 'fighter-preview__health'
  });

  const attackElement = createElement({
    tagName: 'div',
    className: 'fighter-preview__attack'
  });

  const defenseElement = createElement({
    tagName: 'div',
    className: 'fighter-preview__defense'
  });

  titleElement.innerText = 'Name: ' + name;
  healthElement.innerText = 'Health: ' + health;
  attackElement.innerText = 'Attack: ' + attack;
  defenseElement.innerText = 'Defense: ' + defense;

  return [titleElement, healthElement, attackElement, defenseElement];
}
